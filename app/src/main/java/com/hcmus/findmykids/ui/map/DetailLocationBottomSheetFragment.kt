package com.hcmus.findmykids.ui.map

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.hcmus.findmykids.R
import com.hcmus.findmykids.models.Children
import kotlinx.android.synthetic.main.fragment_detail_location_bottom_sheet.*

private const val ARG_PARAM_CHILDREN = "PARAM_CHILDREN"

class DetailLocationBottomSheetFragment : BottomSheetDialogFragment() {
    // TODO: Rename and change types of parameters
    private var children: Children? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            children = it.getParcelable(ARG_PARAM_CHILDREN)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_location_bottom_sheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        initListener()
    }

    private fun initView() {
        children?.let {
            txtName.text = it.name
            txtLocation.text = it.locationName
            Glide.with(this)
                .load(it.avatar)
                .centerCrop()
                .override(resources.getDimensionPixelSize(R.dimen.avatar_size_in_list_children))
                .into(ivAvatar)
        }
    }

    private fun initListener() {
        ivClose.setOnClickListener {
            this.dismiss()
        }

        btnCall.setOnClickListener {
            children?.let {
                val callIntent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + it.phone))
                startActivity(callIntent)
            }
        }
    }

    companion object {
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(children: Children) =
            DetailLocationBottomSheetFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM_CHILDREN, children)
                }
            }
    }
}
