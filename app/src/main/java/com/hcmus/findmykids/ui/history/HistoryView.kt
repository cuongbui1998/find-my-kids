package com.hcmus.findmykids.ui.history

import com.hcmus.findmykids.models.History


/**
 * Created by Cuongbv2 on 01/07/2020.
 */
interface HistoryView {
    fun showNoNetworkConnection()

    fun showLoginError(message: String?)

    fun onSuccess(listHistory: List<History>?)
}