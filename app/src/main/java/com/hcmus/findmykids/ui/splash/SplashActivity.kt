package com.hcmus.findmykids.ui.splash

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.hcmus.findmykids.R
import com.hcmus.findmykids.ui.login.LoginActivity
import com.hcmus.findmykids.ui.main.MainActivity
import com.hcmus.findmykids.ui.phonenumber.PhoneNumberActivity
import com.hcmus.findmykids.utils.UserManager
import kotlin.math.max

class SplashActivity : AppCompatActivity() {

    private var startTime: Long = 0

    private val handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        startTime = System.currentTimeMillis()
        UserManager.readData()
        if (UserManager.isLogin) {
            postDelayNavigationToMain()
        } else {
            postDelayNavigationToLogin()
        }
    }

    private fun postDelayNavigationToMain() {
        val time = System.currentTimeMillis() - startTime
        val delayMillis = max(SPLASH_SCREEN_DURATION - time, 0)
        handler.postDelayed({
            if (UserManager.phoneNumber == "00") {
                startActivity(
                    PhoneNumberActivity.intentFor(
                        this,
                        PhoneNumberActivity.ACTION_ADD
                    ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                )
            } else {
                startActivity(MainActivity.intentFor(this).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
            }
        }, delayMillis)
    }

    private fun postDelayNavigationToLogin() {
        val time = System.currentTimeMillis() - startTime
        val delayMillis = max(SPLASH_SCREEN_DURATION - time, 0)
        handler.postDelayed({
            startActivity(LoginActivity.intentFor(this).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
        }, delayMillis)
    }

    companion object {
        const val SPLASH_SCREEN_DURATION = 1000L

        fun intentFor(context: Context): Intent {
            return Intent(context, SplashActivity::class.java)
        }
    }
}
