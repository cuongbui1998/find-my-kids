package com.hcmus.findmykids.ui.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.hcmus.findmykids.R
import com.hcmus.findmykids.ui.addchildren.AddChildrenActivity
import com.hcmus.findmykids.ui.listchildren.ListChildrenActivity
import com.hcmus.findmykids.ui.phonenumber.PhoneNumberActivity
import com.hcmus.findmykids.ui.splash.SplashActivity
import com.hcmus.findmykids.utils.UserManager
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : Fragment() {

    private lateinit var logoutManager: LogoutManager

    private val logoutResultCallback = object : LogoutManager.LogoutResultCallback {
        override fun onLogoutSuccess() {
            UserManager.deleteData()
            context?.let {
                startActivity(SplashActivity.intentFor(it))
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        initListener()
    }

    private fun init() {
        tvName.text = UserManager.displayName
        tvPhoneNumber.text = UserManager.phoneNumber
        Glide.with(this)
            .load(UserManager.imageUrl)
            .centerCrop()
            .override(resources.getDimensionPixelSize(R.dimen.avatar_size))
            .into(ivAvatar)

        logoutManager = LogoutManager(activity, logoutResultCallback)
    }

    private fun initListener() {
        layoutPhoneNumber.setOnClickListener {
            startActivityForResult(
                activity?.let { it1 ->
                    PhoneNumberActivity.intentFor(
                        it1,
                        PhoneNumberActivity.ACTION_UPDATE
                    )
                },
                REQUEST_CODE
            )
        }

        layoutAddChildren.setOnClickListener {
            context?.let {
                startActivity(AddChildrenActivity.intentFor(it))
            }
        }

        layoutMyChildren.setOnClickListener {
            context?.let {
                startActivity(ListChildrenActivity.intentFor(it))
            }
        }

        layoutLogout.setOnClickListener {
            logoutManager.logoutGoogle()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                tvPhoneNumber.text = UserManager.phoneNumber
            }
        }
    }

    override fun onDestroyView() {
        logoutManager.cleanUp()
        super.onDestroyView()
    }

    companion object {
        const val REQUEST_CODE = 1998
    }
}
