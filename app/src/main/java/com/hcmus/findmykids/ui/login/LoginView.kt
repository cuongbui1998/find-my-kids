package com.hcmus.findmykids.ui.login

import com.hcmus.findmykids.models.User

interface LoginView {

    fun showNoNetworkConnection()

    fun showLoginError(message: String?)

    fun onLoginSuccess(user: User)
}