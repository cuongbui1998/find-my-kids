package com.hcmus.findmykids.ui

interface Presenter<V> {

    fun attachView(view: V)

    fun detachView()
}