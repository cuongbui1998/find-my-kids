package com.hcmus.findmykids.models

import com.google.gson.annotations.SerializedName


/**
 * Created by Cuongbv2 on 01/07/2020.
 */
data class HistoryParams(
    @SerializedName("id")
    val id: String,
    @SerializedName("startDay")
    val startDay: String,
    @SerializedName("endDay")
    val endDay: String
)