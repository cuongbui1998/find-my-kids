package com.hcmus.findmykids.models

import com.google.gson.annotations.SerializedName

data class UpdatePhoneNumberParams(
    @SerializedName("id")
    val id: String,
    @SerializedName("phone")
    val phoneNumber: String
)