package com.hcmus.findmykids.models

import com.google.gson.annotations.SerializedName

data class ListChildrenResponse(
    @SerializedName("listChildren") val listChildren: List<Children>
)