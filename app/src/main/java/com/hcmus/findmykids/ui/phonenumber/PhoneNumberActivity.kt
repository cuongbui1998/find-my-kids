package com.hcmus.findmykids.ui.phonenumber

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.hcmus.findmykids.R
import com.hcmus.findmykids.models.UpdatePhoneNumberResponse
import com.hcmus.findmykids.ui.main.MainActivity
import com.hcmus.findmykids.utils.UserManager
import com.hcmus.findmykids.utils.showShortToast
import kotlinx.android.synthetic.main.activity_phone_number.*

class PhoneNumberActivity : AppCompatActivity(), PhoneNumberView {

    private val phoneNumberPresenter = PhoneNumberPresenter()

    private var action: Int? = ACTION_UPDATE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phone_number)

        init()
        initListener()
    }

    override fun onDestroy() {
        phoneNumberPresenter.detachView()
        super.onDestroy()
    }

    private fun initListener() {
        btnSubmit.setOnClickListener {
            phoneNumberPresenter.updatePhoneNumber(etInputPhoneNumber.text.toString().trim())
        }
    }

    private fun init() {
        phoneNumberPresenter.attachView(this)

        action = intent.getIntExtra(ACTION, ACTION_UPDATE)

        if (action == ACTION_UPDATE) {
            txtInputPhoneTitle.text = getString(R.string.update_phone_number)
            UserManager.phoneNumber?.let {
                etInputPhoneNumber.setText(it, TextView.BufferType.EDITABLE)
            }
        } else {
            txtInputPhoneTitle.text = getString(R.string.input_phone_number_title)
        }
    }

    override fun showNoNetworkConnection() {
        showShortToast(getString(R.string.no_network_connection))
    }

    override fun showError(message: String?) {
        message?.let {
            showShortToast(it)
        }
    }

    override fun onChangePhoneNumberSuccess(response: UpdatePhoneNumberResponse) {
        UserManager.writePhoneNumber(response.phoneNumber)
        if (action == ACTION_UPDATE) {
            setResult(Activity.RESULT_OK)
            finish()
        } else {
            startActivity(MainActivity.intentFor(this))
        }
    }

    companion object {
        const val ACTION_UPDATE = 0;

        const val ACTION_ADD = 1;

        private const val ACTION = "action"

        fun intentFor(context: Context, action: Int): Intent {
            val intent = Intent(context, PhoneNumberActivity::class.java)
            intent.putExtra(ACTION, action)
            return intent
        }
    }
}
