package com.hcmus.findmykids.ui.profile

import android.app.Activity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.hcmus.findmykids.utils.Constants

class LogoutManager(
    private var activity: Activity?,
    private var resultCallback: LogoutResultCallback?
) {
    private var googleSignInClient: GoogleSignInClient? = null

    init {
        initGoogleClient()
    }

    private fun initGoogleClient() {
        if (googleSignInClient == null) {
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(Constants.WEB_CLIENT_ID)
                .requestEmail()
                .build()
            googleSignInClient = GoogleSignIn.getClient(activity!!, gso)
        }
    }

    fun logoutGoogle() {
        googleSignInClient?.signOut()
            ?.addOnCompleteListener {
                onLogoutSuccess()
            }
    }

    private fun onLogoutSuccess() {
        resultCallback?.onLogoutSuccess()
    }

    fun cleanUp() {
        activity = null
        resultCallback = null
    }

    interface LogoutResultCallback {
        fun onLogoutSuccess()
    }
}