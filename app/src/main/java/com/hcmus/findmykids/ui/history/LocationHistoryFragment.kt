package com.hcmus.findmykids.ui.history

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hcmus.findmykids.R
import com.hcmus.findmykids.models.Children
import com.hcmus.findmykids.ui.listchildren.ListChildrenAdapter
import com.hcmus.findmykids.ui.listchildren.ListChildrenPresenter
import com.hcmus.findmykids.ui.listchildren.ListChildrenView
import com.hcmus.findmykids.utils.UserManager
import com.hcmus.findmykids.utils.showShortToast
import kotlinx.android.synthetic.main.activity_list_children.*
import kotlinx.android.synthetic.main.my_toolbar.*


class LocationHistoryFragment : Fragment(), ListChildrenView {

    private var listChildrenAdapter: ListChildrenAdapter? = null

    private var listChildrenPresenter = ListChildrenPresenter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_location_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        initListener()
    }

    override fun onDestroy() {
        listChildrenPresenter.detachView()
        super.onDestroy()
    }

    private fun initView() {
        activity?.let {
            listChildrenPresenter.attachView(this)
            txtTitle.text = getString(R.string.location_history)
            btnBack.visibility = View.GONE
            listChildrenAdapter = ListChildrenAdapter()
            rvListChildren.adapter = listChildrenAdapter
            val layoutManager = LinearLayoutManager(it, RecyclerView.VERTICAL, false)
            rvListChildren.layoutManager = layoutManager
            val dividerItemDecoration = DividerItemDecoration(it, layoutManager.orientation)
            rvListChildren.addItemDecoration(dividerItemDecoration)
            listChildrenPresenter.getListChildren(UserManager.id)
        }
    }

    private fun initListener() {
        listChildrenAdapter?.setOnCLickListener(object : ListChildrenAdapter.OnCLickMoreListener {
            override fun onClick(item: Children) {
                activity?.let {
                    val intent = HistoryActivity.intentFor(it)
                    intent.putExtra(HistoryActivity.KEY_CHILDREN, item)
                    startActivity(intent)
                }

            }

            override fun onCallClick(phoneNumber: String?) {
                phoneNumber?.let {
                    val callIntent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$it"))
                    startActivity(callIntent)
                }
            }

        })
    }

    override fun showNoNetworkConnection() {
        activity?.showShortToast(getString(R.string.no_network_connection))
    }

    override fun showLoginError(message: String?) {
        message?.let {
            activity?.showShortToast(it)
        }
    }

    override fun onSuccess(listChildren: List<Children>?) {
        listChildren?.let {
            listChildrenAdapter?.setData(listChildren)
        }
    }
}
