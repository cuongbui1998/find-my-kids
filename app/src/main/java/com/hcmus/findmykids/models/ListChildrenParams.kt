package com.hcmus.findmykids.models

import com.google.gson.annotations.SerializedName

data class ListChildrenParams(
    @SerializedName("idUser")
    val id: String?
)