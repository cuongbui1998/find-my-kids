package com.hcmus.findmykids.ui.addchildren

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.hcmus.findmykids.R
import com.hcmus.findmykids.utils.showShortToast
import kotlinx.android.synthetic.main.activity_add_children.*
import kotlinx.android.synthetic.main.my_toolbar.*

class AddChildrenActivity : AppCompatActivity(), AddChildrenView {
    private val addChildrenPresenter = AddChildrenPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_children)

        init()
        initListener()
    }

    override fun onDestroy() {
        addChildrenPresenter.detachView()
        super.onDestroy()
    }

    private fun init() {
        addChildrenPresenter.attachView(this)

        txtTitle.text = getString(R.string.add_children)
    }

    private fun initListener() {
        btnBack.setOnClickListener {
            finish()
        }

        btnSubmit.setOnClickListener {
            etCode.text.toString().let { code ->
                if (code == "") {
                    showShortToast(getString(R.string.please_enter_code))
                } else {
                    addChildrenPresenter.addChildren(code)
                }
            }
        }
    }

    override fun showNoNetworkConnection() {
        showShortToast(getString(R.string.no_network_connection))
    }

    override fun showError(message: String?) {
        message?.let {
            showShortToast(it)
        }
    }

    override fun onSuccess() {
        showShortToast(getString(R.string.connect_children_success))
    }

    override fun onDuplicateAction() {
        showShortToast(getString(R.string.have_connected_before))
    }

    override fun onIncorrectCode() {
        showShortToast(getString(R.string.code_is_incorrect))
    }

    companion object {
        fun intentFor(context: Context): Intent {
            return Intent(context, AddChildrenActivity::class.java)
        }
    }
}
