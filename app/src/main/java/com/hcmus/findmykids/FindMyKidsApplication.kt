package com.hcmus.findmykids

import android.app.Application
import android.content.Context

class FindMyKidsApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        lateinit var instance: FindMyKidsApplication
    }
}