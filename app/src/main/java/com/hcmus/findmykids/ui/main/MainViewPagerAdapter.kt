package com.hcmus.findmykids.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.hcmus.findmykids.ui.history.LocationHistoryFragment
import com.hcmus.findmykids.ui.map.MapFragment
import com.hcmus.findmykids.ui.profile.ProfileFragment

class MainViewPagerAdapter(fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val numPage = 3

    private var arrFragment: MutableList<Fragment> =
        mutableListOf(MapFragment(), LocationHistoryFragment(), ProfileFragment())

    override fun getCount(): Int = numPage

    override fun getItem(position: Int): Fragment {
        return arrFragment[position]
    }
}