package com.hcmus.findmykids.models

import com.google.gson.annotations.SerializedName

class AddChildrenParams (
    @SerializedName("idUser")
    val idUser: String?,
    @SerializedName("code")
    val code: String?
)
