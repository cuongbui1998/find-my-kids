package com.hcmus.findmykids.ui.listchildren

import com.hcmus.findmykids.models.Children

interface ListChildrenView {
    fun showNoNetworkConnection()

    fun showLoginError(message: String?)

    fun onSuccess(listChildren: List<Children>?)
}