package com.hcmus.findmykids.ui.phonenumber

import com.hcmus.findmykids.models.UpdatePhoneNumberResponse

interface PhoneNumberView {
    fun showNoNetworkConnection()

    fun showError(message: String?)

    fun onChangePhoneNumberSuccess(response: UpdatePhoneNumberResponse)
}