package com.hcmus.findmykids.models

import com.google.gson.annotations.SerializedName

class AddChildrenResponse(
    @SerializedName("status")
    val status: Int?
)