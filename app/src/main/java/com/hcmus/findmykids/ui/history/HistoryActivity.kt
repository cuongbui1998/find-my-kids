package com.hcmus.findmykids.ui.history

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hcmus.findmykids.R
import com.hcmus.findmykids.models.Children
import com.hcmus.findmykids.models.History
import com.hcmus.findmykids.utils.StringUtils
import com.hcmus.findmykids.utils.showShortToast
import kotlinx.android.synthetic.main.activity_history.*
import kotlinx.android.synthetic.main.my_toolbar.*
import java.text.SimpleDateFormat
import java.util.*


class HistoryActivity : AppCompatActivity(), HistoryView {
    private var children: Children? = null

    private var startDay: String = ""

    private var endDay: String = ""

    private lateinit var datePickerFromDialog: DatePickerDialog

    private lateinit var datePickerToDialog: DatePickerDialog

    private val calendar = Calendar.getInstance()

    private val historyPresenter = HistoryPresenter()

    private var historyAdapter: HistoryAdapter? = null

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        children = intent.getParcelableExtra(KEY_CHILDREN)

        initView()
        initListener()
    }

    private fun initListener() {
        btnBack.setOnClickListener {
            finish()
        }
        etDateFrom.setOnClickListener {
            datePickerFromDialog.show()
        }
        etDateTo.setOnClickListener {
            datePickerToDialog.show()
        }

        btnShowHistory.setOnClickListener {
            historyPresenter.getLocationHistory(children?.id, startDay, endDay)
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun initView() {
        datePickerFromDialog = DatePickerDialog(
            this@HistoryActivity,
            DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                val realMonth = month + 1
                startDay =
                    "$year-${StringUtils.formatMonth(realMonth)}-${StringUtils.formatDayOfMonth(
                        dayOfMonth
                    )}"
                etDateFrom.setText(startDay, TextView.BufferType.EDITABLE)
            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        datePickerToDialog = DatePickerDialog(
            this@HistoryActivity,
            DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                val realMonth = month + 1
                endDay =
                    "$year-${StringUtils.formatMonth(realMonth)}-${StringUtils.formatDayOfMonth(
                        dayOfMonth
                    )}"
                etDateTo.setText(endDay, TextView.BufferType.EDITABLE)
            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        historyPresenter.attachView(this)
        children?.let {
            txtTitle.text = it.name
            Glide.with(this)
                .load(it.avatar)
                .centerCrop()
                .override(resources.getDimensionPixelSize(R.dimen.avatar_size_in_list_children))
                .into(ivAvatar)
        }
        val date: Date = Calendar.getInstance().time
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        startDay = sdf.format(date)
        etDateFrom.setText(startDay, TextView.BufferType.EDITABLE)
        endDay = sdf.format(date)
        etDateTo.setText(endDay, TextView.BufferType.EDITABLE)
        historyAdapter = HistoryAdapter()
        rvHistory.adapter = historyAdapter
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rvHistory.layoutManager = layoutManager
        val dividerItemDecoration = DividerItemDecoration(this, layoutManager.orientation)
        rvHistory.addItemDecoration(dividerItemDecoration)
    }

    override fun onDestroy() {
        historyPresenter.detachView()
        super.onDestroy()
    }

    companion object {
        const val KEY_CHILDREN = "key_children"

        fun intentFor(context: Context): Intent {
            return Intent(context, HistoryActivity::class.java)
        }
    }

    override fun showNoNetworkConnection() {
        showShortToast(getString(R.string.no_network_connection))
    }

    override fun showLoginError(message: String?) {
        message?.let {
            showShortToast(it)
        }
    }

    override fun onSuccess(listHistory: List<History>?) {
        historyAdapter?.setData(listHistory)
    }
}