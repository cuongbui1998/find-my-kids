package com.hcmus.findmykids.utils

import android.content.Context
import android.content.SharedPreferences
import com.hcmus.findmykids.models.User

object UserManager {

    private const val DEFAULT_STRING = ""

    var id: String? = null

    var displayName: String? = null

    var imageUrl: String? = null

    var phoneNumber: String? = null

    var email: String? = null

    var isLogin: Boolean = false

    private const val USER_PREF = "user_prefs"

    private const val ID = "id"

    private const val DISPLAY_NAME = "display_name"

    private const val EMAIL = "email"

    private const val PHONE_NUMBER = "phone_number"

    private const val IMAGE_URL = "image_url"

    private val sharedPreferences: SharedPreferences? =
        appContext().getSharedPreferences(USER_PREF, Context.MODE_PRIVATE)

    fun readData() {
        sharedPreferences?.let {
            id = it.getString(ID, DEFAULT_STRING)
            displayName = it.getString(DISPLAY_NAME, DEFAULT_STRING)
            imageUrl = it.getString(IMAGE_URL, DEFAULT_STRING)
            phoneNumber = it.getString(PHONE_NUMBER, DEFAULT_STRING)
            email = it.getString(EMAIL, DEFAULT_STRING)
        }
        isLogin = id != DEFAULT_STRING
    }

    fun deleteData() {
        sharedPreferences?.edit()?.let {
            it.putString(ID, DEFAULT_STRING)
            it.putString(DISPLAY_NAME, DEFAULT_STRING)
            it.putString(PHONE_NUMBER, DEFAULT_STRING)
            it.putString(IMAGE_URL, DEFAULT_STRING)
            it.putString(EMAIL, DEFAULT_STRING)
            it.apply()
            id = DEFAULT_STRING
            displayName = DEFAULT_STRING
            imageUrl = DEFAULT_STRING
            phoneNumber = DEFAULT_STRING
            email = DEFAULT_STRING
            isLogin = false
        }
    }

    fun writeData(user: User?) {
        sharedPreferences?.edit()?.let {
            id = user?.id
            it.putString(ID, id)
            displayName = user?.displayName
            it.putString(DISPLAY_NAME, displayName)
            email = user?.email
            it.putString(EMAIL, email)
            imageUrl = user?.imageUrl
            it.putString(IMAGE_URL, imageUrl)
            phoneNumber = user?.phoneNumber
            it.putString(PHONE_NUMBER, phoneNumber)
            it.apply()
        }
    }

    fun writePhoneNumber(newPhoneNumber: String?) {
        sharedPreferences?.edit()?.let {
            phoneNumber = newPhoneNumber
            it.putString(PHONE_NUMBER, phoneNumber)
            it.apply()
        }
    }
}