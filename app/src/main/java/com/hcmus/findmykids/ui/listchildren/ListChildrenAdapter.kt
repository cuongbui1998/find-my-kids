package com.hcmus.findmykids.ui.listchildren

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hcmus.findmykids.R
import com.hcmus.findmykids.models.Children
import kotlinx.android.synthetic.main.list_children_item.view.*

class ListChildrenAdapter : RecyclerView.Adapter<ListChildrenAdapter.ChildrenViewHolder>() {

    private val items: MutableList<Children> = mutableListOf()

    private lateinit var onCLickListener: OnCLickMoreListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChildrenViewHolder {
        return ChildrenViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_children_item, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ChildrenViewHolder, position: Int) {
        holder.bind(items[position])
    }

    fun setData(list: List<Children>?) {
        items.clear()
        list?.apply {
            items.addAll(this)
        }
        notifyDataSetChanged()
    }


    fun setOnCLickListener(onCLickMoreListener: OnCLickMoreListener) {
        this.onCLickListener = onCLickMoreListener
    }

    fun getItems(): MutableList<Children> = items

    inner class ChildrenViewHolder(
        view: View
    ) : RecyclerView.ViewHolder(view) {

        private val tvName = itemView.tvName

        private val ivAvatar = itemView.ivAvatar

        private val btnCall = itemView.btnCall

        fun bind(item: Children) {
            tvName.text = item.name
            Glide.with(itemView)
                .load(item.avatar)
                .centerCrop()
                .override(itemView.resources.getDimensionPixelSize(R.dimen.avatar_size_in_list_children))
                .into(ivAvatar)

            itemView.setOnClickListener {
                onCLickListener.onClick(item)
            }

            btnCall.setOnClickListener {
                onCLickListener.onCallClick(item.phone)
            }
        }
    }

    interface OnCLickMoreListener {
        fun onClick(item: Children)
        fun onCallClick(phoneNumber: String?)
    }
}