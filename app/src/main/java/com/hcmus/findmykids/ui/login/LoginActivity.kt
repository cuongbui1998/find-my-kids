package com.hcmus.findmykids.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.hcmus.findmykids.R
import com.hcmus.findmykids.models.User
import com.hcmus.findmykids.ui.main.MainActivity
import com.hcmus.findmykids.ui.phonenumber.PhoneNumberActivity
import com.hcmus.findmykids.utils.UserManager
import com.hcmus.findmykids.utils.isNetworkConnected
import com.hcmus.findmykids.utils.showShortToast
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity(), LoginView {

    private lateinit var loginManager: LoginManager

    private var loginPresenter = LoginPresenter()

    private val resultCallback = object : LoginManager.LoginResultCallback {
        override fun onLoginError(errorMessage: String) {
            showShortToast(errorMessage)
        }

        override fun onLoginSuccess(accessToken: String) {
            progress.visibility = View.VISIBLE
            btnLogin.isClickable = false
            loginPresenter.loginWithGoogle(accessToken)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        init()
        initListener()
    }

    private fun init() {
        loginManager = LoginManager(this, resultCallback)
        loginPresenter.attachView(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        data?.let {
            if (loginManager.onActivityResult(requestCode, resultCode, it)) {
                return
            }
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun showNoNetworkConnection() {
        showShortToast(getString(R.string.no_network_connection))
    }

    override fun showLoginError(message: String?) {
        message?.let {
            showShortToast(it)
        }
    }

    override fun onLoginSuccess(user: User) {
        UserManager.writeData(user)
        if (user.phoneNumber == "00") {
            startActivity(
                PhoneNumberActivity.intentFor(
                    this,
                    PhoneNumberActivity.ACTION_ADD
                ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            )
        } else {
            startActivity(MainActivity.intentFor(this).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
        }
    }

    override fun onDestroy() {
        loginPresenter.detachView()
        loginManager.cleanUp()
        super.onDestroy()
    }

    private fun initListener() {
        btnLogin.setOnClickListener {
            if (isNetworkConnected()) {
                loginManager.loginGoogle()
            } else {
                showNoNetworkConnection()
            }
        }
    }

    companion object {
        fun intentFor(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }
}
