package com.hcmus.findmykids.ui.addchildren

import com.hcmus.findmykids.models.AddChildrenParams
import com.hcmus.findmykids.models.AddChildrenResponse
import com.hcmus.findmykids.service.FMKService
import com.hcmus.findmykids.service.RetrofitClient
import com.hcmus.findmykids.ui.BasePresenter
import com.hcmus.findmykids.utils.UserManager
import com.hcmus.findmykids.utils.isNetworkError
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class AddChildrenPresenter : BasePresenter<AddChildrenView>() {
    private val fmkService: FMKService = RetrofitClient.fmkInstance.create(
        FMKService::class.java
    )

    fun addChildren(code: String) {
        UserManager.id?.let { idUser ->
            val addChildrenParams = AddChildrenParams(idUser, code)
            fmkService.addChildren(addChildrenParams)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<AddChildrenResponse> {
                    lateinit var disposable: Disposable
                    override fun onComplete() {
                        disposable.dispose()
                    }

                    override fun onSubscribe(d: Disposable) {
                        disposable = d
                    }

                    override fun onNext(t: AddChildrenResponse) {
                        t.status.let {
                            when (it) {
                                -1 -> {
                                    view?.onIncorrectCode()
                                }
                                1 -> {
                                    view?.onSuccess()
                                }
                                2 -> {
                                    view?.onDuplicateAction()
                                }
                                else -> {
                                    //TODO
                                }
                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e.isNetworkError()) {
                            view?.showNoNetworkConnection()
                        } else {
                            view?.showError(e.message)
                        }
                    }

                })
        }
    }
}