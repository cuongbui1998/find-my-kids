package com.hcmus.findmykids.ui.addchildren

interface AddChildrenView {
    fun showNoNetworkConnection()

    fun showError(message: String?)

    fun onSuccess()

    fun onDuplicateAction()

    fun onIncorrectCode()
}