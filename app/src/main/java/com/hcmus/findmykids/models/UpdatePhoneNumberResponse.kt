package com.hcmus.findmykids.models

import com.google.gson.annotations.SerializedName

data class UpdatePhoneNumberResponse (
    @SerializedName("phone")
    val phoneNumber: String?
)