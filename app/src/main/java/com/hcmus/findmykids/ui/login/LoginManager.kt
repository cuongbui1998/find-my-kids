package com.hcmus.findmykids.ui.login

import android.app.Activity
import android.content.Intent
import android.content.res.Resources
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.hcmus.findmykids.R
import com.hcmus.findmykids.utils.Constants
import com.hcmus.findmykids.utils.appContext
import com.hcmus.findmykids.utils.isNetworkConnected

class LoginManager(
    private var activity: Activity?,
    private var resultCallback: LoginResultCallback?
) {

    private var resources: Resources = appContext().resources

    private var googleSignInClient: GoogleSignInClient? = null

    init {
        initGoogleClient()
    }

    private fun initGoogleClient() {
        if (googleSignInClient == null) {
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(Constants.WEB_CLIENT_ID)
                .requestEmail()
                .build()
            googleSignInClient = GoogleSignIn.getClient(activity!!, gso)
        }
    }

    fun loginGoogle() {
        if (googleSignInClient == null) {
            onError(resources.getString(R.string.google_connection_error))
            return
        }
        val signInIntent = googleSignInClient!!.signInIntent
        activity?.startActivityForResult(
            signInIntent,
            LOGIN_GG_REQUEST_CODE
        )
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent): Boolean {
        if (LOGIN_GG_REQUEST_CODE == requestCode) {
            if (Activity.RESULT_OK == resultCode) {
                val task: Task<GoogleSignInAccount> =
                    GoogleSignIn.getSignedInAccountFromIntent(data)
                handleGGSignInResult(task)
            } else if (Activity.RESULT_CANCELED == resultCode) {
                onError(resources.getString(R.string.cancel_sign_in_gg))
            }
            return true
        }
        return false
    }

    private fun handleGGSignInResult(result: Task<GoogleSignInAccount>) {

        try {
            val signInAccount = result.getResult(ApiException::class.java)
            if (signInAccount != null) {
                val idToken = signInAccount.idToken

                if (!idToken.isNullOrEmpty()) {
                    onSuccess(idToken)
                } else {
                    onError(
                        resources.getString(
                            R.string.message_login_fail,
                            resources.getString(R.string.google_plus),
                            "NoTK"
                        )
                    )
                }
            } else {
                onError(
                    resources.getString(
                        R.string.message_login_fail,
                        resources.getString(R.string.google_plus),
                        "NoAcc"
                    )
                )
            }
        } catch (e: ApiException) {
            if (isNetworkConnected()) {
                onError(
                    resources.getString(
                        R.string.message_login_fail,
                        resources.getString(R.string.google_plus),
                        (result.exception)
                    )
                )
            } else {
                onError(resources.getString(R.string.no_network_connection))
            }
        }

    }

    fun cleanUp() {
        activity = null
        resultCallback = null
    }

    private fun onError(errorMessage: String) {
        resultCallback?.onLoginError(errorMessage)
    }

    private fun onSuccess(token: String) {
        resultCallback?.onLoginSuccess(token)
    }

    interface LoginResultCallback {
        fun onLoginError(errorMessage: String)

        fun onLoginSuccess(accessToken: String)
    }

    companion object {
        private const val LOGIN_GG_REQUEST_CODE = 9001
    }
}