package com.hcmus.findmykids.service

import com.hcmus.findmykids.models.*
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.PUT

interface FMKService {
    @POST("api/users/auth/google")
    fun login(@Body loginParams: LoginParams): Observable<User>

    @PUT("api/users/phone")
    fun updatePhoneNumber(@Body updatePhoneNumberParams: UpdatePhoneNumberParams): Observable<UpdatePhoneNumberResponse>

    @POST("api/kids")
    fun addChildren(@Body addChildrenParams: AddChildrenParams): Observable<AddChildrenResponse>

    @POST("api/kids/list")
    fun getListChildren(@Body listChildrenParams: ListChildrenParams): Observable<ListChildrenResponse>

    @POST("api/history/location-by-time")
    fun getLocationHistory(@Body historyParams: HistoryParams): Observable<HistoryResponse>
}