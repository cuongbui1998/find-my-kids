package com.hcmus.findmykids.ui.main

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.hcmus.findmykids.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewPagerAdapter: MainViewPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        initListener()
    }

    private fun initListener() {
        bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.bottom_navigation_item_location -> viewPager.setCurrentItem(0, true)
                R.id.bottom_navigation_item_history -> viewPager.setCurrentItem(1, true)
                R.id.bottom_navigation_item_profile -> viewPager.setCurrentItem(2, true)
            }
            true
        }

        viewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                bottomNavigation.menu.getItem(position).isChecked = true
                super.onPageSelected(position)
            }
        })
    }

    private fun init() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            viewPagerAdapter = MainViewPagerAdapter(supportFragmentManager)
            viewPager.adapter = viewPagerAdapter
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_PERMISSION_CODE
                )
            }
        }

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CALL_PHONE
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            viewPagerAdapter = MainViewPagerAdapter(supportFragmentManager)
            viewPager.adapter = viewPagerAdapter
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                    arrayOf(
                        Manifest.permission.CALL_PHONE),
                    REQUEST_PERMISSION_CODE
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_PERMISSION_CODE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    viewPagerAdapter = MainViewPagerAdapter(supportFragmentManager)
                    viewPager.adapter = viewPagerAdapter
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    finish()
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    companion object {
        private const val REQUEST_PERMISSION_CODE = 1998
        fun intentFor(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }
}
