package com.hcmus.findmykids.ui.phonenumber

import com.hcmus.findmykids.models.UpdatePhoneNumberParams
import com.hcmus.findmykids.models.UpdatePhoneNumberResponse
import com.hcmus.findmykids.service.FMKService
import com.hcmus.findmykids.service.RetrofitClient
import com.hcmus.findmykids.ui.BasePresenter
import com.hcmus.findmykids.utils.UserManager
import com.hcmus.findmykids.utils.isNetworkError
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class PhoneNumberPresenter : BasePresenter<PhoneNumberView>() {

    private val fmkService: FMKService = RetrofitClient.fmkInstance.create(
        FMKService::class.java
    )

    fun updatePhoneNumber(phoneNumber: String) {
        UserManager.id?.let { id ->
            val param = UpdatePhoneNumberParams(id, phoneNumber)
            fmkService.updatePhoneNumber(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<UpdatePhoneNumberResponse> {
                    lateinit var disposable: Disposable
                    override fun onComplete() {
                        disposable.dispose()
                    }

                    override fun onSubscribe(d: Disposable) {
                        disposable = d
                    }

                    override fun onNext(t: UpdatePhoneNumberResponse) {
                        view?.onChangePhoneNumberSuccess(t)
                    }

                    override fun onError(e: Throwable) {
                        if (e.isNetworkError()) {
                            view?.showNoNetworkConnection()
                        } else {
                            view?.showError(e.message)
                        }
                    }

                })
        }
    }
}