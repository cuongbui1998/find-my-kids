package com.hcmus.findmykids.models

import com.google.gson.annotations.SerializedName

data class LoginParams(
    @SerializedName("idToken")
    val idToken: String
)