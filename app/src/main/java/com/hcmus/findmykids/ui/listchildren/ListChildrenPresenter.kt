package com.hcmus.findmykids.ui.listchildren

import com.hcmus.findmykids.models.ListChildrenParams
import com.hcmus.findmykids.models.ListChildrenResponse
import com.hcmus.findmykids.service.FMKService
import com.hcmus.findmykids.service.RetrofitClient
import com.hcmus.findmykids.ui.BasePresenter
import com.hcmus.findmykids.utils.isNetworkError
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ListChildrenPresenter : BasePresenter<ListChildrenView>() {
    private val fmkService: FMKService = RetrofitClient.fmkInstance.create(
        FMKService::class.java
    )

    fun getListChildren(id: String?) {
        id?.let {
            val listChildrenParams = ListChildrenParams(it)
            fmkService.getListChildren(listChildrenParams)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ListChildrenResponse> {
                    lateinit var disposable: Disposable
                    override fun onComplete() {
                        disposable.dispose()
                    }

                    override fun onSubscribe(d: Disposable) {
                        disposable = d
                    }

                    override fun onNext(t: ListChildrenResponse) {
                        view?.onSuccess(t.listChildren)
                    }

                    override fun onError(e: Throwable) {
                        if (e.isNetworkError()) {
                            view?.showNoNetworkConnection()
                        } else {
                            view?.showLoginError(e.message)
                        }
                    }

                })
        }
    }
}