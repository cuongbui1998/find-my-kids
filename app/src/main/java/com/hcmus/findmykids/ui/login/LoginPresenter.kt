package com.hcmus.findmykids.ui.login

import com.hcmus.findmykids.models.LoginParams
import com.hcmus.findmykids.models.User
import com.hcmus.findmykids.service.FMKService
import com.hcmus.findmykids.service.RetrofitClient
import com.hcmus.findmykids.ui.BasePresenter
import com.hcmus.findmykids.utils.isNetworkError
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class LoginPresenter : BasePresenter<LoginView>() {
    private val fmkService: FMKService = RetrofitClient.fmkInstance.create(
        FMKService::class.java
    )

    fun loginWithGoogle(idToken: String) {
        val loginParam = LoginParams(idToken)
        fmkService.login(loginParam)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<User>{
                lateinit var disposable: Disposable
                override fun onComplete() {
                    disposable.dispose()
                }

                override fun onSubscribe(d: Disposable) {
                    disposable = d
                }

                override fun onNext(t: User) {
                    view?.onLoginSuccess(t)
                }

                override fun onError(e: Throwable) {
                    if(e.isNetworkError()){
                        view?.showNoNetworkConnection()
                    }else{
                        view?.showLoginError(e.message)
                    }
                }

            })
    }
}