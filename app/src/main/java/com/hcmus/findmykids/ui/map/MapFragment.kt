package com.hcmus.findmykids.ui.map

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.hcmus.findmykids.R
import com.hcmus.findmykids.models.Children
import com.hcmus.findmykids.ui.listchildren.ListChildrenPresenter
import com.hcmus.findmykids.ui.listchildren.ListChildrenView
import com.hcmus.findmykids.utils.StringUtils
import com.hcmus.findmykids.utils.UserManager
import com.hcmus.findmykids.utils.showShortToast
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class MapFragment : Fragment(), ListChildrenView, GoogleMap.OnMarkerClickListener {

    private lateinit var googleMap: GoogleMap

    private lateinit var mapView: MapView

    private lateinit var rootView: View

    private val handler = Handler()

    private val listChildrenPresenter = ListChildrenPresenter()

    private lateinit var locationThread: Thread

    private lateinit var list: List<Children>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_map, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listChildrenPresenter.attachView(this)
        mapView = rootView.findViewById(R.id.map)
        mapView.onCreate(savedInstanceState)
        mapView.onResume()
        try {
            MapsInitializer.initialize(activity!!.applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mapView.getMapAsync { map ->
            googleMap = map
            googleMap.isMyLocationEnabled = true
        }
    }

    private fun fetchLocation() {
        listChildrenPresenter.getListChildren(UserManager.id)
        locationThread = Thread(LocationRunnable())
        locationThread.start()
    }

    override fun onResume() {
        mapView.onResume()
        fetchLocation()
        super.onResume()
    }

    override fun onPause() {
        mapView.onPause()
        locationThread.interrupt()
        super.onPause()
    }

    override fun onDestroy() {
        mapView.onDestroy()
        listChildrenPresenter.detachView()
        super.onDestroy()
    }

    override fun onLowMemory() {
        mapView.onLowMemory()
        super.onLowMemory()
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        if (p0 != null) {
            val index = p0.tag.toString().toInt()
            activity?.let {
                if (index >= 0 && index < list.size) {
                    val bottomSheet = DetailLocationBottomSheetFragment.newInstance(
                        list[index]
                    )
                    bottomSheet.show(it.supportFragmentManager, null)
                }
            }
        }
        return false
    }

    override fun showNoNetworkConnection() {
        activity?.showShortToast(getString(R.string.no_network_connection))
    }

    override fun showLoginError(message: String?) {
        message?.let {
            activity?.showShortToast(it)
        }
    }

    override fun onSuccess(listChildren: List<Children>?) {
        listChildren?.let {
            list = listChildren
            googleMap.clear()
            var location: LatLng
            if (listChildren.size > 1) {
                val builder = LatLngBounds.Builder()
                for (children in listChildren) {
                    if (children.latitude != null && children.longitude != null) {
                        location = LatLng(children.latitude, children.longitude)
                        val date = getDate(children.currentTime)
                        val dateString = transferDateToString(date)
                        val marker = googleMap.addMarker(
                            MarkerOptions().position(location)
                                .title(dateString)
                        )
                        marker.tag = listChildren.indexOf(children)
                        builder.include(location)
                    }
                }
                val bounds = builder.build()
                val cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, MARKER_PADDING_MAP)
                googleMap.animateCamera(cameraUpdate)
            } else {
                for (children in listChildren) {
                    if (children.latitude != null && children.longitude != null) {
                        location = LatLng(children.latitude, children.longitude)
                        val date = getDate(children.currentTime)
                        val dateString = transferDateToString(date)
                        googleMap.addMarker(
                            MarkerOptions().position(location)
                                .title(dateString)
                        )
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 15.0F))
                    }
                }
            }
        }
        googleMap.setOnMarkerClickListener(this)
    }

    @SuppressLint("SimpleDateFormat")
    private fun getDate(dateString: String?): Date? {
        val dateStringAfterFormat = StringUtils.formatDate(dateString)
        return try {
            SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateStringAfterFormat)
        } catch (e: ParseException) {
            null
        }
    }

    private fun transferDateToString(date: Date?): String {
        return if (date != null) {
            resources.getString(R.string.was_here_at) + " " + date.toString()
        } else {
            ""
        }
    }

    inner class LocationRunnable() : Runnable {
        override fun run() {
            try {
                do {
                    Thread.sleep(LOCATION_INTERVAL)
                    handler.post {
                        listChildrenPresenter.getListChildren(UserManager.id)
                    }
                } while (true)
            } catch (e: InterruptedException) {
                //TODO
            }
        }
    }

    companion object {
        const val MARKER_PADDING_MAP = 200
        const val LOCATION_INTERVAL = 20L * 1000L
    }
}
