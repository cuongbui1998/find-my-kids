package com.hcmus.findmykids.ui.history

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hcmus.findmykids.R
import com.hcmus.findmykids.models.History
import kotlinx.android.synthetic.main.location_history_item.view.*


/**
 * Created by Cuongbv2 on 01/07/2020.
 */
class HistoryAdapter : RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>() {

    private val items: MutableList<History> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        return HistoryViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.location_history_item, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        holder.bind(items[position])
    }

    fun setData(list: List<History>?) {
        items.clear()
        list?.apply {
            items.addAll(this)
        }
        notifyDataSetChanged()
    }

    fun getItems(): MutableList<History> = items

    inner class HistoryViewHolder(
        view: View
    ) : RecyclerView.ViewHolder(view) {

        private val txtTime = itemView.txtTime

        private val txtLocation = itemView.txtLocation

        fun bind(item: History) {
            txtTime.text = item.time
            txtLocation.text = item.locationName
        }
    }
}