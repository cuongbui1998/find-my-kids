package com.hcmus.findmykids.ui.listchildren

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hcmus.findmykids.R
import com.hcmus.findmykids.models.Children
import com.hcmus.findmykids.ui.history.HistoryActivity
import com.hcmus.findmykids.utils.UserManager
import com.hcmus.findmykids.utils.showShortToast
import kotlinx.android.synthetic.main.activity_list_children.*
import kotlinx.android.synthetic.main.my_toolbar.*

class ListChildrenActivity : AppCompatActivity(), ListChildrenView {

    private var listChildrenAdapter: ListChildrenAdapter? = null

    private var listChildrenPresenter = ListChildrenPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_children)

        initView()
        initListener()
    }

    override fun onDestroy() {
        listChildrenPresenter.detachView()
        super.onDestroy()
    }

    private fun initListener() {
        btnBack.setOnClickListener {
            finish()
        }

        listChildrenAdapter?.setOnCLickListener(object : ListChildrenAdapter.OnCLickMoreListener {
            override fun onClick(item: Children) {
                //TODO
            }

            override fun onCallClick(phoneNumber: String?) {
                phoneNumber?.let {
                    val callIntent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$it"))
                    if (ActivityCompat.checkSelfPermission(
                            this@ListChildrenActivity,
                            Manifest.permission.CALL_PHONE
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return
                    } else {
                        startActivity(callIntent)
                    }
                }
            }

        })
    }

    private fun initView() {
        listChildrenPresenter.attachView(this)
        txtTitle.text = getString(R.string.my_children)
        listChildrenAdapter = ListChildrenAdapter()
        rvListChildren.adapter = listChildrenAdapter
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rvListChildren.layoutManager = layoutManager
        val dividerItemDecoration = DividerItemDecoration(this, layoutManager.orientation)
        rvListChildren.addItemDecoration(dividerItemDecoration)
        listChildrenPresenter.getListChildren(UserManager.id)
    }

    override fun showNoNetworkConnection() {
        showShortToast(getString(R.string.no_network_connection))
    }

    override fun showLoginError(message: String?) {
        message?.let {
            showShortToast(it)
        }
    }

    override fun onSuccess(listChildren: List<Children>?) {
        listChildren?.let {
            listChildrenAdapter?.setData(listChildren)
        }
    }

    companion object {
        fun intentFor(context: Context): Intent {
            return Intent(context, ListChildrenActivity::class.java)
        }
    }
}
