package com.hcmus.findmykids.ui.history

import com.hcmus.findmykids.models.HistoryParams
import com.hcmus.findmykids.models.HistoryResponse
import com.hcmus.findmykids.service.FMKService
import com.hcmus.findmykids.service.RetrofitClient
import com.hcmus.findmykids.ui.BasePresenter
import com.hcmus.findmykids.utils.isNetworkError
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


/**
 * Created by Cuongbv2 on 01/07/2020.
 */
class HistoryPresenter : BasePresenter<HistoryView>() {
    private val fmkService: FMKService = RetrofitClient.fmkInstance.create(
        FMKService::class.java
    )

    fun getLocationHistory(id: String?, startDay: String, endDay: String) {
        id?.let {
            val historyParams = HistoryParams(it, startDay, endDay)
            fmkService.getLocationHistory(historyParams)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<HistoryResponse> {
                    lateinit var disposable: Disposable
                    override fun onComplete() {
                        disposable.dispose()
                    }

                    override fun onSubscribe(d: Disposable) {
                        disposable = d
                    }

                    override fun onNext(t: HistoryResponse) {
                        view?.onSuccess(t.history)
                    }

                    override fun onError(e: Throwable) {
                        if (e.isNetworkError()) {
                            view?.showNoNetworkConnection()
                        } else {
                            view?.showLoginError(e.message)
                        }
                    }

                })
        }
    }
}