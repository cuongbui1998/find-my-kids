package com.hcmus.findmykids.models

import com.google.gson.annotations.SerializedName


/**
 * Created by Cuongbv2 on 01/07/2020.
 */
data class History(
    @SerializedName("time")
    val time: String,
    @SerializedName("latitude")
    val latitude: String,
    @SerializedName("longitude")
    val longitude: String,
    @SerializedName("locationName")
    val locationName: String
)