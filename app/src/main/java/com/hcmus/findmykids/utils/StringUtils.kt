package com.hcmus.findmykids.utils

object StringUtils {
    fun formatDate(date: String?): String {
        var indexT = 0
        if (date != null) {
            for (character in date) {
                if (character == 'T') {
                    break
                }
                indexT++
            }
            return date.substring(0, indexT) + " " + date.substring(indexT + 1, date.length - 5)
        }
        return ""
    }

    fun transferMarkerIdToInt(id: String): Int {
        val newId = id.substring(1, id.length)
        return try {
            newId.toInt()
        } catch (e: NumberFormatException) {
            0
        }
    }

    fun formatDayOfMonth(day: Int): String {
        return if (day < 10) {
            "0$day"
        } else {
            day.toString()
        }
    }

    fun formatMonth(month: Int): String {
        return if (month < 10) {
            "0$month"
        } else {
            month.toString()
        }
    }
}