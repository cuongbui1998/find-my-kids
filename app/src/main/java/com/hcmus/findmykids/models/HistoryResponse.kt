package com.hcmus.findmykids.models

import com.google.gson.annotations.SerializedName


/**
 * Created by Cuongbv2 on 01/07/2020.
 */
data class HistoryResponse(
    @SerializedName("status")
    val status: Int,
    @SerializedName("history")
    val history: List<History>
)